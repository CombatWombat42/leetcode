struct ListNode
{
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

#include <iostream>

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution
{

public:
    ListNode *addTwoNumbers(ListNode *l1, ListNode *l2)
    {
        int val1, val2, overflow = 0;
        ListNode *firstDigit, *currentL1, *currentL2;
        ListNode *prevDig = nullptr;

        currentL1 = l1;
        currentL2 = l2;
        while (!(currentL1 == nullptr && currentL2 == nullptr))
        {

            if (currentL1 == nullptr)
            {
                val1 = 0;
            }
            else
            {
                val1 = currentL1->val;
            }
            if (currentL2 == nullptr)
            {
                val2 = 0;
            }
            else
            {
                val2 = currentL2->val;
            }
            ListNode* nextDig = new ListNode;
            if (prevDig == nullptr)
            {
                firstDigit = nextDig;
                prevDig = nextDig;
            }
            else
            {
                prevDig->next = nextDig;
            }
            nextDig->val = (val1 + val2 + overflow) % 10;
            if (val1 + val2  + overflow >= 10)
            {
                overflow = 1;
            }
            else
            {
                overflow = 0;
            }
            if (currentL1 != nullptr)
            {
                currentL1 = currentL1->next;
            }
            else
            {
                currentL1 = nullptr;
            }

            if (currentL2 != nullptr)
            {
                currentL2 = currentL2->next;
            }
            else
            {
                currentL2 = nullptr;
            }
            prevDig = nextDig;
        }
        if(overflow == 1)
        {
            ListNode* nextDig = new ListNode;
            prevDig->next = nextDig;
            nextDig->val = 1;
        }
        return firstDigit;
    }
};

int main(void)
{
    ListNode d11, d12, d13, d21, d22, d23, *sol;
    d11.next = &d12;
    d12.next = &d13;
    d13.next = nullptr;
    d21.next = &d22;
    d22.next = &d23;
    d23.next = nullptr;

    d11.val = 2;
    d12.val = 4;
    d13.val = 3;

    d21.val = 5;
    d22.val = 6;
    d23.val = 5;

    Solution solution;

    sol = solution.addTwoNumbers(&d11, &d21);
    while(sol != nullptr)
    {
        std::cout << sol->val;
        sol = sol->next;
    }
    std::cout << std::endl;
}
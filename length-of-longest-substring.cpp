#include <string>
#include <map>
#include <iostream>

class Solution
{
public:
    int lengthOfLongestSubstring(std::string s)
    {

        std::map<char, int> substring_map;
        int longestSubstring = 0;
        int currentLength = 0;
        //another possible option is to make an array of arrays of all characters, then run through all the arrays to see what the largest gap is

        for (auto string_iterator = s.cbegin(); string_iterator != s.cend(); string_iterator++)
        {
            auto map_iterator = substring_map.find(*string_iterator);
            if (map_iterator != substring_map.end() && string_iterator != s.cend())
            {
                //found a duplicate what is the current length? if longer than current longest record it
                if (currentLength > longestSubstring)
                {
                    longestSubstring = currentLength;
                }
                currentLength -= std::distance(substring_map.begin(), map_iterator) + 1;
                // drop the previous occurance and everything before it from the map
                // NOTE: THIS DOES NOT DO THE RIGHT THING, THE MAP IS ORDERD BY KEYS NOT INSERTION TIME
                substring_map.erase(substring_map.begin(), (++map_iterator));
                // continue walking through the string
                // mark beginning location of this substring as 1+ previous location
                //string_iterator--;
            }
            auto string_index =  string_iterator - s.cbegin();
            substring_map[*string_iterator] = string_index;
            currentLength++;
        }
        if (currentLength > longestSubstring)
        {
            longestSubstring = currentLength;
        }

        return longestSubstring;
    }
};

int main(void)
{
    std::map<std::string, int> test_cases;

    test_cases["asdfasdf"] = 4;
    test_cases[""] = 0;
    test_cases["bbbbbbb"] = 1;
    test_cases["pwwkew"] = 3;
    test_cases["qwertyuiopasdfghjklzxcvbnmq"] = 26;
    test_cases[" "] = 1;
    test_cases["2"] = 1;
    test_cases["au"] = 2;
    test_cases["dvdf"] = 3;
    test_cases["dvdfd"] = 3;
    test_cases["ynyo"] = 3;

    Solution solver;
    for (auto case_iterator = test_cases.cbegin(); case_iterator != test_cases.cend(); case_iterator++)
    {
        auto case_val = solver.lengthOfLongestSubstring(case_iterator->first);
        if (case_iterator->second != case_val)
            std::cout << "failed \"" << case_iterator->first << "\" expected " << case_iterator->second << " but got " << case_val << std::endl;
    }
}
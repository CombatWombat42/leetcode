#include <iostream>

struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}

    /**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
    class Solution
    {
    private:
        int ans = 0;
        std::vector<int> search(TreeNode *root, int distance)
        {
            std::vector<int> leafsDistance = {};
            if (root == nullptr)
            {
                return leafsDistance;
            }

            if ((root->left == nullptr) && (root->right == nullptr))
            {
                // I am a leaf I have one distance from myself
                leafsDistance.push_back(1);
                return leafsDistance;
            }

            auto leftLeaveList = search(root->left, distance);
            auto rightLeaveList = search(root->right, distance);

            for (int leftLeaves = 0; leftLeaves < leftLeaveList.size(); leftLeaves++)
            {
                for (int rightLeaves = 0; rightLeaves < rightLeaveList.size(); rightLeaves++)
                {
                    if (leftLeaveList[leftLeaves] + rightLeaveList[rightLeaves] <= distance)
                    {
                        ans++;
                    }
                }
            }

            for (int leftLeaves = 0; leftLeaves < leftLeaveList.size(); leftLeaves++)
            {
                leafsDistance.push_back(++leftLeaveList[leftLeaves]);
            }

            for (int rightLeaves = 0; rightLeaves < rightLeaveList.size(); rightLeaves++)
            {
                leafsDistance.push_back(++rightLeaveList[rightLeaves]);
            }
            return leafsDistance;
        }

    public:
        int countPairs(TreeNode *root, int distance)
        {
            search(root, distance);
            return ans;
        }
    };
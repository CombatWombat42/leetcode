#include <vector>
#include <iostream>

struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

/**
 *  NOTES: looking at the solutions online none of them are taking this approach, I think i'm over complicating things for myself and am going to move on to another problem
 * 
 */

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution
{
private:
    std::vector<int> compareTrees(TreeNode *root1, TreeNode *root2)
    {
        std::vector<int> retVec;

        //base case is my left node is null and my val is lower than the val of the other tree
        if(root1->left == nullptr)
        {
            if(root1->val < root2->val)
            {
                retVec.push_back(root1->val);
            }
            else
            {
                auto tempVec = compareTrees(nullptr,root2->left);
                retVec.insert(retVec.end(),tempVec.begin(),tempVec.end());
            }
            
            if(root2->left == nullptr)
            {
                if(root2->val < root1.val)
                {
                    retVec.push_back(root2->val);
                }
                else
                {
                    auto tempVec = compareTrees(root1->left,nullptr);
                    retVec.insert(retVec.end(),tempVec.begin(),tempVec.end());
                }
                
            }

            
        }

        if (root1->val > root2->val)
        {
            auto 
        }
    }

public:
    std::vector<int> getAllElements(TreeNode *root1, TreeNode *root2)
    {
        std::vector<int> ans;
        ans.push_back(20);
        ans.push_back(23);
        return ans;
    }
};

int main(void)
{
    TreeNode zero, one, one2, two, three, four;
    zero.val = 0;
    one.val = 1;
    one2.val = 1;
    two.val = 2;
    three.val = 3;
    four.val = 4;

    two.left = &one2;
    two.right = &four;

    one.left = &zero;
    one.right = &three;

    Solution solver;
    auto ans = solver.getAllElements(&two, &one);
    for (int x : ans)
    {
        std::cout << x << " ";
    }
    std::cout << std::endl;
}

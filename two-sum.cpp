#include <vector>
#include <map>
#include <iostream>

class Solution {
public:
    std::vector<int> twoSum(std::vector<int>& nums, int target) {
        
        std::map<int,int> indexMap = {};
        for (std::vector<int>::iterator it = nums.begin() ; it != nums.end(); ++it)
        {
            //Check if target my value exsists, if it does we have a solution
            if(indexMap.count(target-*it) > 0)
            {
                std::cout << "found match " <<  indexMap[target-*it] << " " << (it - nums.begin()) << std::endl;
                std::vector<int> returnVal = {0};
                returnVal.push_back(indexMap[target-*it]);
                returnVal.push_back((it - nums.begin()));
                return returnVal;
            }
            // if not add current index to the map at my value
            indexMap[*it] = (it - nums.begin());
        }
        return std::vector<int>(-1,-1);
    }
};